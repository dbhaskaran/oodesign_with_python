#Define an Employee class with attributes/methods 
#Define a Manager class and a Finance Manager  class that inherit from the Employee class
#

class Employee:
    def __init__(self, name, job, pay, grade=10):
        self.name = name
        self.job = job
        self.pay = pay
        self.grade = grade

    def __str__(self):
        return '[Person: %s, %s with %s salary]' % ( self.name, self.job, self.pay)

class Manager (Employee):
    def give_raise(self, employee, amount):
        employee.pay = employee.pay + amount

class HRManager (Manager):
    def view_employee_info(self, employee):
        return '[HR Sees that %s is at grade %s ]' % (employee.name, employee.grade)


if __name__ == '__main__':
    bob = Employee('Bob', 'Accountant', 75000)
    #print (bob)

    mary = Manager('Mary', 'Accounting Manager', 100000, 11)
    #print (mary)

    mary.give_raise(bob, 10000)
    #print (bob)

    alice = HRManager('Alice', 'HR Manager', 100000, 11)
    #print (alice.view_employee_info (bob))

    print ('Printing all the __str__ method results')
    print (bob)
    print (mary)
    print (alice)

    print ('Printing class info')
    print (bob.__class__)
    print (mary.__class__)
    print (alice.__class__)

    print ('Printing class attritbutes')
    print (bob.__dict__.keys())
    print (mary.__dict__.keys())
    print (alice.__dict__.keys())

    print ('Printing class attritbutes')
    print (dir(bob))
    print (dir(mary))
    print (dir(alice))
