import datetime

class BankAccount:
    MIN_BALANCE = 10000

    def __init__(self, owner, account_number, balance=0):
        self.owner = owner
        self.account_number = account_number
        self.balance = balance
        self.created_at = datetime.date.today()

account1 = BankAccount("Deepak", 1001001, 10000)

print (account1.owner)
print (account1.account_number)
print (account1.created_at.year)
