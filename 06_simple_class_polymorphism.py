import datetime


class BankAccount():
    MIN_BALANCE = -1

    def __init__(self, owner, account_number, ssn, balance=0):
        self.owner = owner
        self.account_number = account_number
        if balance < self.MIN_BALANCE:
            raise ValueError("Balance too small!")
        else:
            self.balance = balance

        self.created_at = datetime.date.today()
        self.__ssn = ssn

    def get_account_type(self):
        return ("bank account")

class CommercialBankAccount(BankAccount):

    def get_account_type(self):
        return ("commercial bank account")

class SavingsBankAccount(BankAccount):

    def get_account_type(self):
        return ("savings bank account")

account1 = BankAccount("Deepak", 1001001, "111-11-1111", 1000)
account2 = CommercialBankAccount("Tara Inc", 2001001, "211-11-1111", 1000)
account3 = SavingsBankAccount("Surya", 3001001, "311-11-1111", 1000)

print (account1.get_account_type())
print (account2.get_account_type())
print (account2.owner)
print (account3.get_account_type())
print (account3.owner)
