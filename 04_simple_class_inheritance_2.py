import datetime
from simple_class_inheritance import BankAccount

class CommercialBankAccount(BankAccount):

    def get_account_type(self):
        return ("commercial bank account")

class SavingsBankAccount(BankAccount):

    def get_account_type(self):
        return ("savings bank account")

account1 = BankAccount("Deepak", 1001001, "111-11-1111", 1000)
account2 = CommercialBankAccount("Tara Inc", 2001001, "211-11-1111", 1000)
account3 = SavingsBankAccount("Surya", 3001001, "311-11-1111", 1000)

print (account1.get_account_type())
print (account2.get_account_type())
print (account2.owner)
print (account3.get_account_type())
print (account3.owner)
