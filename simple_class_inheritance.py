import datetime

class BankAccount:
    MIN_BALANCE = -1

    def __init__(self, owner, account_number, ssn, balance=0):
        self.owner = owner
        self.account_number = account_number
        if balance < self.MIN_BALANCE:
            raise ValueError("Balance too small!")
        else:
            self.balance = balance

        self.created_at = datetime.date.today()
        self.__ssn = ssn

    def get_account_type(self):
        return ("bank account")


class CommercialBankAccount(BankAccount):

    def get_account_type(self):
        return ("commercial bank account")

