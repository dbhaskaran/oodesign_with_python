import datetime

class BankAccount:
    MIN_BALANCE = -1

    def __init__(self, owner, account_number, ssn, balance=0):
        self.owner = owner
        self.account_number = account_number
        if balance < self.MIN_BALANCE:
            raise ValueError("Balance too small!")
        else:
            self.balance = balance

        self.created_at = datetime.date.today()
        self.__ssn = ssn

    def check_ssn_match (self, ssn1):
        if self.__ssn == ssn1:
            return ("match")
        else:
            return ("not-a-match")

account1 = BankAccount("Deepak", 1001001, "111-11-1111", 1000)

print (account1.owner)
print (account1.account_number)
print (account1.created_at.year)
print (account1.ssn)
